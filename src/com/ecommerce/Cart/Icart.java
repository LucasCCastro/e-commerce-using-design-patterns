package com.ecommerce.Cart;


public interface Icart {
    public void add(Observer observer);

    public void remove(Observer observer);

    public void notification();
}