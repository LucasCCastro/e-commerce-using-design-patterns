package com.ecommerce.Cart;

public interface Observer {
    public void update();
}
