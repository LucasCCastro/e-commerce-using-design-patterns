package com.ecommerce.Cart;

import com.ecommerce.Product.Product;
import com.ecommerce.Product.ProductShell;

import java.util.ArrayList;
import java.util.List;

public class Cart implements Icart, Observer {

    private List<Observer> observers;

    public List<Product> productList;
    private static Cart uniqueInstance = null;

    private Cart() {
        productList = new ArrayList<>();
        this.observers = new ArrayList<>();
    }

    public static Cart getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Cart();
        }
        return uniqueInstance;
    }

    public boolean add(String productName) {
        List<Product> productShell = ProductShell.getInstance().getAvailableProducts();

        for (Product pd : productShell) {
            if (pd.getName().equals(productName)) {
                this.productList.add(pd);
                notification();
                return true;
            }
        }
        System.out.println("No Products Available With This Name");
        return false;
    }

    public boolean delete(String productToRemove) {

        for (Product pd : productList) {
            if (pd.getName().equals(productToRemove)) {
                this.productList.remove(pd);
                notification();
                return true;
            }
        }
        System.out.println("No Products int the cart With This Name");
        return false;
    }

    public void listProducts() {
        if (productList.isEmpty()) {
            System.out.println("- Your cart is currently empty.");
        }
        productList.forEach(System.out::println);
    }

    public double calculateOrderPrice() {
        double finalAmount = 0.0;
        for (Product pd : productList) {
            finalAmount += pd.getValue();
        }
        return finalAmount;
    }

    @Override
    public void add(Observer observer) {
    }

    @Override
    public void remove(Observer observer) {
        int index = observers.indexOf(observer);
        if (index > -1) {

            System.out.println("");
            System.out.println("Removed: " + observer.getClass().getName());
            System.out.println("");

            observers.remove(observer);
        }
    }

    @Override
    public void notification() {
        System.out.println("");
        System.out.println("New update");
        System.out.println("");
        this.update();
    }

    @Override
    public void update() {
        System.out.println("Updated Info:");
        listProducts();
    }

}
