package com.ecommerce.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductShell {
    List<Product> availableProducts;

    private static ProductShell uniqueInstance = null;

    public ProductShell() {
        availableProducts = new ArrayList<>();
        populateProductShell();
    }

    public static ProductShell getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new ProductShell();
        }
        return uniqueInstance;
    }

    private void populateProductShell() {
        availableProducts.add(new Product("Playstation", 4999));
        availableProducts.add(new Product("Xbox", 4500));
        availableProducts.add(new Product("Chair", 1000));
        availableProducts.add(new Product("Fifa", 100));
        availableProducts.add(new Product("GTA", 100));
        availableProducts.add(new Product("Mouse", 30));
        availableProducts.add(new Product("Keyboard", 50));
        availableProducts.add(new Product("Monitor", 350));
        availableProducts.add(new Product("Nvidia", 999));
        availableProducts.add(new Product("Rizen", 500));
    }

    public void listProducts() {
        availableProducts.forEach(System.out::println);
    }

    public List<Product> getAvailableProducts() {
        return availableProducts;
    }
}
