package com.ecommerce.Product;

public class Product {

    private String name;
    private double value;

    public Product(String name, double value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "- '" + name + '\'' +
                ", price = " + value;
    }

}