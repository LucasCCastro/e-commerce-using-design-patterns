package com.ecommerce;

import com.ecommerce.Cart.Cart;
import com.ecommerce.Payment.PaymentAbstraction;
import com.ecommerce.Payment.PaymentFactory;
import com.ecommerce.Payment.PaymentMethodsEnum;
import com.ecommerce.Product.ProductShell;

import java.util.Scanner;

public class UserInterface {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int choice;

        System.out.println("\nHello, welcome to the ES E-Commerce!");
        do {
            System.out.println("\nWhat would you like to do?");
            System.out.println("1) Check the available products");
            System.out.println("2) Review was is into your cart");
            System.out.println("3) Add a product to your cart");
            System.out.println("4) Delete a product that is in your cart");
            System.out.println("5) Proceed to finish your order");
            System.out.println("0) Close the application");
            System.out.print("Enter your choice: ");
            choice = in.nextInt();

            switch (choice) {
                case 1: {
                    System.out.println("\nThese are the available products in the store:");
                    ProductShell.getInstance().listProducts();
                    break;
                }
                case 2: {
                    System.out.println("\nHere you can find the products that are in your cart:");
                    Cart.getInstance().listProducts();
                    break;
                }
                case 3: {
                    System.out.print("\nPlease enter the name of a desired product to add it to your cart: ");
                    String newProduct = in.next();
                    boolean wasAdded = Cart.getInstance().add(newProduct);
                    if (wasAdded) {
                        System.out.println("\n" + newProduct + " was added to your cart!");
                    }

                    if (!wasAdded) {
                        System.out.println("\n" + newProduct + " was not found in the available products.");
                    }
                    break;
                }
                case 4: {
                    System.out.print("\nPlease, write exactly the name of the product that you wish to remove from your cart: ");
                    String productToRemove = in.next();

                    boolean wasDeleted = Cart.getInstance().delete(productToRemove);
                    if (wasDeleted) {
                        System.out.println("\n" + productToRemove + " was removed from your cart!");
                    }

                    if (!wasDeleted) {
                        System.out.println("\n" + productToRemove + " was not found in your cart.");
                    }
                    break;
                }
                case 5: {
                    if (Cart.getInstance().calculateOrderPrice() == 0.0) {
                        System.out.println("\nYour cart is currently empty!");
                        break;
                    }

                    System.out.println("\nYour order cost is " + Cart.getInstance().calculateOrderPrice() + ".");
                    System.out.println("How would you like to pay it?");
                    System.out.println("1) Debit Card");
                    System.out.println("2) Credit Card");
                    System.out.println("3) Transfer");
                    System.out.println("4) Picpay");
                    System.out.print("Select the option: ");
                    int paymentMethod = in.nextInt();

                    System.out.print("\nNow inform your first name: ");
                    String firstName = in.next();

                    System.out.print("and your social number: ");
                    String socialNumber = in.next();

                    PaymentMethodsEnum paymentEnum;
                    if (paymentMethod == 1)
                        paymentEnum = PaymentMethodsEnum.DEBIT;
                    else if (paymentMethod == 2)
                        paymentEnum = PaymentMethodsEnum.CREDIT;
                    else if (paymentMethod == 3)
                        paymentEnum = PaymentMethodsEnum.TRANSFER;
                    else if (paymentMethod == 4)
                        paymentEnum = PaymentMethodsEnum.PICPAY;
                    else {
                        System.out.println("The option entered does not exist.");
                        break;
                    }

                    PaymentFactory paymentFactory = new PaymentFactory();
                    PaymentAbstraction paymentClass = paymentFactory.getPaymentMethodClass(firstName, socialNumber, paymentEnum);
                    paymentClass.paymentInteraction();
                    Cart.getInstance().productList.clear();
                    break;
                }
                case 0: {
                    System.out.println("\nThank you for using our platform! :D");
                    break;
                }
            }
        } while (choice != 0);

    }
}
