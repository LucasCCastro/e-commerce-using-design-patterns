package com.ecommerce.Payment;

public enum PaymentMethodsEnum {
    CREDIT,
    DEBIT,
    TRANSFER,
    PICPAY
}
