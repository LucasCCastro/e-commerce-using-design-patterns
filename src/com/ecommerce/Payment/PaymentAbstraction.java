package com.ecommerce.Payment;

public abstract class PaymentAbstraction {

    public String customerName;
    public String customerSocialNumber;
    public PaymentMethodsEnum paymentMethod;

    public void paymentInteraction(){}
}
