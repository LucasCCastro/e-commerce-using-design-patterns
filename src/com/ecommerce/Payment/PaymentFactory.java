package com.ecommerce.Payment;

import com.ecommerce.Payment.Alternatives.CreditCard;
import com.ecommerce.Payment.Alternatives.DebitCard;
import com.ecommerce.Payment.Alternatives.Picpay;
import com.ecommerce.Payment.Alternatives.Transfer;

public class PaymentFactory {

    public PaymentAbstraction getPaymentMethodClass(String customerName, String customerSocialNumber, PaymentMethodsEnum paymentMethod) {

        if (paymentMethod.equals(PaymentMethodsEnum.CREDIT))
            return new CreditCard(customerName, customerSocialNumber, paymentMethod);

        if (paymentMethod.equals(PaymentMethodsEnum.DEBIT))
            return new DebitCard(customerName, customerSocialNumber, paymentMethod);

        if (paymentMethod.equals(PaymentMethodsEnum.TRANSFER))
            return new Transfer(customerName, customerSocialNumber, paymentMethod);

        if (paymentMethod.equals(PaymentMethodsEnum.PICPAY))
            return new Picpay(customerName, customerSocialNumber, paymentMethod);

        return null;
    }
}