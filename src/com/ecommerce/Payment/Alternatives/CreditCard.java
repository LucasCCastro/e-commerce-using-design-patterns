package com.ecommerce.Payment.Alternatives;

import com.ecommerce.Payment.PaymentAbstraction;
import com.ecommerce.Payment.PaymentMethodsEnum;

import java.util.Scanner;

public class CreditCard extends PaymentAbstraction {

    Scanner in = new Scanner(System.in);
    private String cardNumber;
    private String expirationDate;

    public CreditCard(String customerName, String customerSocialNumber, PaymentMethodsEnum paymentMethod) {
        this.customerName = customerName;
        this.customerSocialNumber = customerSocialNumber;
        this.paymentMethod = paymentMethod;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void paymentInteraction() {
        System.out.print("\nTo pay your order, please inform your Card Number: ");
        String customerCardNumber = in.next();
        System.out.print("Now enter the card expiration date: ");
        String customerCardExpirationDate = in.next();

        this.setCardNumber(customerCardNumber);
        this.setExpirationDate(customerCardExpirationDate);

        System.out.println("\nYour credit card info was confirmed and your order was successful!");
        System.out.println("Thanks for shopping with us!");
    }
}
