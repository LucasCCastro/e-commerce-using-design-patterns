package com.ecommerce.Payment.Alternatives;

import com.ecommerce.Payment.PaymentAbstraction;
import com.ecommerce.Payment.PaymentMethodsEnum;

public class Transfer extends PaymentAbstraction {

    // Company Transfer Details
    private final String bankAccount = "123.456.789-10";
    private final String bankNumber = "25";
    private final String companyEnterpriseNumber = "987.654.321";

    public Transfer(String customerName, String customerSocialNumber, PaymentMethodsEnum paymentMethod) {
        this.customerName = customerName;
        this.customerSocialNumber = customerSocialNumber;
        this.paymentMethod = paymentMethod;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public String getCompanyEnterpriseNumber() {
        return companyEnterpriseNumber;
    }

    public void paymentInteraction() {
        System.out.println("\nTo pay your order, please make a transfer using this info:");
        System.out.println("Bank Account: " + bankAccount);
        System.out.println("Bank Number: " + bankNumber);
        System.out.println("Company Enterprise Number: " + companyEnterpriseNumber);

        System.out.println("After we receive the confirmation of your transfer, we will confirm your order by email!");
        System.out.println("Thanks for shopping with us!");
    }
}
