package com.ecommerce.Payment.Alternatives;

import com.ecommerce.Payment.PaymentAbstraction;
import com.ecommerce.Payment.PaymentMethodsEnum;

public class Picpay extends PaymentAbstraction {

    private final String companyPicpayUsername = "ES-ECOMMERCE";

    public Picpay(String customerName, String customerSocialNumber, PaymentMethodsEnum paymentMethod) {
        this.customerName = customerName;
        this.customerSocialNumber = customerSocialNumber;
        this.paymentMethod = paymentMethod;
    }

    public String getCompanyPicpayUsername() {
        return companyPicpayUsername;
    }

    public void paymentInteraction() {
        System.out.println("\nTo pay your order, please make a transfer to this PicPay account:");
        System.out.println("- " + companyPicpayUsername);

        System.out.println("After we receive the confirmation of your transfer, we will confirm your order by email!");
        System.out.println("Thanks for shopping with us!");
    }
}
